# Metapipe on AWS experiments

In here: stuff for running MetaPipe and metapipe benchmarks on AWS.

License: MIT.

The way this is supposed to work is that there are a bunch of prerequisites you need to put in place on AWS, mostly manually, and then there are some mostly automated scripts for running metapipe benchmarks.
The way it actually works is that the automated benchmark scripts didn't materialize, at least not to the degree envisioned.
Which is not a big deal, because it turns out that experiments are very expensive and take a long time, so automation does not give us huge benefits.

This README walks you through all necessary steps from the creation of an AWS account to running metapipe and experiments.


## "Metapipe on AWS" setup summary

We rig up a small and inexpensive SSH-only bastion EC2 instance on AWS.
On that bastion we can use the AWS API and CLI without further ado.
We use the instance for all sorts of management and rigging tasks.
To this end, we clone this repository there.

For Metapipe, we make AWS EMR clusters on demand.
An EMR cluster boots up with a compatible version of Spark and is provisioned with metapipe stuff automatically, using a boot-time provisioning script we supply.
When a cluster is started, it sits idle until we start metapipe's execution manager on the cluster's master node.
Execution manager continually fetches jobs from the metapipe frontend at jobs.metapipe.uit.no, and submits them to Spark on the cluster.
We have to terminate the cluster ourselves, either by a script through the AWS API or manually in the AMR console.

Scripts can automate pretty much everything of the above with relative ease, since almost everything can be done through the AWS CLI and API and CloudFormation.
I haven't written any (larger) scripts yet, though.

We do initial setup of the AWS account, resources and settings manually.


## Initial rigging on AWS

You need an AWS account.
We take it from there - approximately, that is, since I may have forgotten about some pretty trivial steps right after activating the AWS account.


### IAM roles, keys, security groups

We need some AWS IAM user accounts, security, authentication and firewall security groups stuff in place.
Some of it we must do beforehand ourselves, other stuff is made automatically by amazon, for instance when we launch our first EMR cluster.

IIRC, the AWS console nags you about creating IAM users. Create one, give it all accesses (there's an Admin role), call it "metapipe".

We need an IAM role that allows an EC2 instance to access all AWS functions.
In the IAM console: new role -> ec2 instance role -> attach "administrator access".
Call it "aws-admin".

Make a Key Pair in the EC2 console (Network and Security).
Call it "amazon\_emr".
Store the private key safely on your machine.
(We'll later put it on our bastion instance, too.)

Make a security group for external SSH access in the EC2 console (Network and Security). 
Call it "ssh external", add inbound rules for SSH access from the (external, non-AWS) IPs you use.

Make another security group for internal SSH access.
Call it "ssh internal", add inbound rules for SSH access from the "ssh internal" security group.
(You might have to save and then edit the group so that you get a group id you can refer to in the inbound rule you edit.)

Some security groups and IAM roles are created automatically by AWS when we make our first EMR cluster.
So we do just that.
In the EMR console, we rig a small cluster with one master node and one worker.
Click on "create cluster".
Non-default options are:

- no logging
- number of instances: 2
- EC2 key pair: choose amazon\_emr, the key we created before

Just wait until the cluster is up and running, and terminate it.
AWS will have created some security groups and IAM roles and instance profiles.
Every subsequent EMR cluster we create will use these by default.


### S3 bucket

We need an S3 bucket.
If we don't already have one from the prior steps, just make one.
No need for any non-defaults.
Make sure it's not public.
Make sure it's in the same region as everything else.
Remember the name.


### Optional: elastic File System share

We can use an AWS "Elastic File System" share to store metapipe's temporary directory.
This share can be mounted on all EMR cluster nodes.
This is optional, however, and I haven't used this in most of the experiments I ran.

First create a security group for EFS.
Go to the EC2 console, in "Network and Security" choose "Security Groups", click "Create Security Group", call it "efs", and add inbound rules for NFS from both EMR groups and the default group.

Then go to the EFS console, click "create file system", and choose the following non-default settings:

- add a tag "Name: metapipe-efs"
- add the security group "efs" in all availability zones, remove the default security group
- use the "general purpose" performance mode (TODO: we should test which one works better for us)

Remember the DNS name.


### Bastion EC2 instance

We rig up a small and inexpensive EC2 instance for more rigging and management.

We can optionally also start all experiments (i.e. "run metapipectl") on that instance, but in that case we have to choose a more expensive instance type with sufficient RAM (several GB).
Experiments can be started from any unixy machine, doesn't have to be on AWS.
So if you have a unixy machine at hand, go with the cheap instance.

In the EC2 console, click "launch instance".
Select amazon linux, t2.micro instance, select the amazon\_emr SSH key.
Non-default advanced options: pick one subnet and remember it (want to use the same for EMR clusters later), IAM role aws-admin, add a 200GB general purpose EBS volume with delete on termination at /dev/sdb, attach default and both ssh security groups.
Spin up the instance.

SSH to it, and then:
```bash
aws configure
# leave AWS credentials empty - the instance has access through the IAM role
# choose and remember your region. We use us-east-2 here
# default output doesn't matter, but choose "text"

sudo yum install -y git
ssh-keygen
```

Now get your git credentials to the machine, whatever is necessary to clone this repository.
Usually this means just copying parts from your own local ~/.gitconfig file over to the bastion.

Now clone this repository.
If you need a deploy key because you can't acceess the git server (which might be the case for instance on source.uit.no if you didn't ssh -A to the bastion instance), use the public key from the keypair you just made.
(You can add deploy keys in the project's settings on source.uit.no, but you have to be project owner or master to do that.)
```bash
git clone git@source.uit.no:sua/metapipe-on-aws.git
```

Some local configuration for our scripts - outcomment the bit about the EFS share if you don't have that:
```bash
cd metapipe-on-aws/config
cp env.template env
vim env
```

Fill in the blanks!

Now scp the private key file of our SSH key "amazon\_emr" to `~/metapipe-on-aws/config/amazon\_emr.pem`.

The rest of the local setup can be done automatically by running the bootstrap\_bastion script - outcomment the bits about the EFS share if you don't have that:
```bash
cd metapipe-on-aws
./bootstrap_bastion.sh
```

Check the content of that script for further details on the stuff we rig initially on the bastion host.
Note that the script is not idempotent.

After the script has run, you can activate the python virtualenv, load a bunch of useful METAPIPE\_ environment variables, and add the amazon\_emr ssh key to ssh agent, by doing this:
```bash
cd metapipe-on-aws
. ./activate_env.sh
```

Install utilities (you probably want at least screen or tmux) at will.


### metapipectl config and frontend credentials

On the bastion instance, the bootstrap scripts copied metapipectl config files to ~/.metapipe/.
We need to fill in the metapipe frontend client secret in the credentials.json file.
If you don't have this, contact Alex.


### Metapipe distribution and temp dir

Download and unpack the metapipe distribution and dependencies to the EFS share (optional) and the extra block device, and from there to S3.
Downloading and chmod of the dependencies (>67GB, zillions of files) takes a very long time, so you might want to run this in a tmux or screen session.
Scripts for downloading can be found in the dist/ directory.

Note that these instructions were written in a bit of a haste, so proceed with caution and common sense - you want to end up with a file structure on EFS (optional) and S3 that the config/cluster\_bootstrap.sh script can digest.

#### optional: using EFS

SSH to the bastion, then:
```bash
source ~/metapipe-on-aws/config/env
cd /mnt/efs
sudo chmod 777 /mnt/efs

# bunch of metapipe distribution files
/home/ec2-user/metapipe-on-aws/dist/getem.sh
# conf.json will later be put in place on the EMR nodes by a script


# metapipe temp directory, used by all cluster nodes
mkdir metapipe-temp
chmod 777 metapipe-temp

# metapipe dependencies - lots of stuff
mkdir metapipe-dependencies
cd metapipe-dependencies
tar xvf ../metapipe-dependencies.tar.gz
cd ..
chown -R 498:500 metapipe-dependencies
# 498:500 is hadoop:hadoop on EMR clusters later

# as of the time of this writing there are some bugs in the dependencies directory
# in deps/tools/, chmod mga directory to 755
# in deps/databases/marref, chmod 644 *
```

#### using extra block device and S3

SSH to the bastion, then:
```bash
source ~/metapipe-on-aws/config/env
cd /mnt/metapipe

# bunch of metapipe distribution files
/home/ec2-user/metapipe-on-aws/dist/getem.sh

# metapipe dependencies - lots of stuff, unpacked, fixed, then packed again
tar xvf metapipe-dependencies.tar.gz
chown -R 498:500 package
# 498:500 is hadoop:hadoop on EMR clusters later
# as of the time of this writing there are some bugs in the dependencies directory
# in deps/tools/, chmod mga directory to 755
# in deps/databases/marref, chmod 644 *
sudo tar czvf metapipe-deps.tar.gz package	
sudo chown ec2-user:ec2-user metapipe-deps.tar.gz
rm -rf metapipe-dependencies*

#upload everything to S3
/home/ec2-user/metapipe-on-aws/dist/upload_to_s3.sh
```


### EMR cluster bootstrap script on S3

When we launch EMR clusters, we have a bootstrap script that configures metapipe-specific things.
EMR wants this bootstrap script in S3, so we upload it there.

First, we have to customize it a bit, by coding in the EFS share.

On the bastion host:
```bash
cd metapipe-on-aws/config/
cp cluster_bootstrap.sh.template cluster_bootstrap.sh
vim cluster_bootstrap.sh
```

Replace `${METAPIPE_EFS_DNS_NAME}` and `${METAPIPE_BUCKET}` with their values (stored in config/env).

Then upload the file to S3:
```bash
. ./env
aws s3 cp cluster_bootstrap.sh s3://${METAPIPE_BUCKET}/rigging/cluster_bootstrap.sh
```



## Rigging an EMR cluster and running metapipe on it

This is the bit that one would want to automate when running lots of experiments. Here's how it's done manually.


### Launch metapipectl on bastion instance (or your own computer if bastion instance doesn't have sufficient RAM)

First, scp or download your fasta file to ~/metapipectl

Then:
```bash
cd metapipectl
java -jar metapipectl-assembly-0.1-SNAPSHOT.jar submit --export-merged-genbank false --force -t metapipe-lars-aws PATH-TO-FASTA-FILE
```

The program will run until all is finished, will then download results from metapipe.uit.no and store them in PWD (some subdirectory).
That might be a lot of data.
It is OK to ctrl+c or otherwise terminate metapipectl after it has uploaded the job to metapipe.uit.no.

About the parameters:
```
--export-merged-genbank false # ask agych
--force # rerun whole pipeline every time job is submitted
-t custom tag, IMPORTANT, OTHERWISE WE STEAL JOBS FROM THE FRONTEND!
```

Check out the metapipectl/ directory in this repository, there are scripts that run metapipectl for experiments.
(At the time of this writing, a grand total of one.)


### Launch EMR cluster

In the EMR console, configure a new cluster using advanced options like this (only non-default options mentioned):

- choose emr-4.9.2 to get spark 1.6.3 which has scala 2.10 (requirement for current metapipe)
    - check Spark and Ganglia in addition to the other selected programs
- add the following software settings JSON, which is directly taken from https://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-configure-apps.html#configuring-java8
```
[
    {
        "Classification": "hadoop-env", 
        "Configurations": [
            {
                "Classification": "export", 
                "Configurations": [], 
                "Properties": {
                    "JAVA_HOME": "/usr/lib/jvm/java-1.8.0"
                }
            }
        ], 
        "Properties": {}
    }, 
    {
        "Classification": "spark-env", 
        "Configurations": [
            {
                "Classification": "export", 
                "Configurations": [], 
                "Properties": {
                    "JAVA_HOME": "/usr/lib/jvm/java-1.8.0"
                }
            }
        ], 
        "Properties": {}
    }
]
```
- make sure you have the right Network and EC2 subnet (you want the same as bastion and EFS share)
- Instances (examples):
    - m4.4xlarge instances are similar to cpouta instances agych used
    - m4.2xlarge have half cpu, ram
    - c4.4xlarge instances good enough according to alex raknes (more CPU but less RAM)
- meaningful cluster name helps, we need neither logging nor debugging and definitely not termination protection
- choose bootstrap action: s3://our-metapipe-bucket/rigging/cluster\_bootstrap.sh, no arguments
- use the amazon\_emr key pair
- in security groups, add external ssh group for master, and internal ssh group for master, core and task.

When the cluster nodes are ready, you can SSH to the master.


### Run metapipe execution manager on EMR cluster master node

There's a lot of material on google docs about this (check the experiment logs for instance), but here's one example:

SSH to the cluster master, then
```bash
cd metapipe
java -jar executionManager-assembly-0.1-SNAPSHOT.jar command-backed -e test-executor -t metapipe-lars-aws -- spark-submit --driver-memory 20g --executor-memory 10g --deploy-mode cluster --master yarn --supervise -v --conf spark.task.cpus=8 --conf spark.executor.cores=8 /efs/workflow-assembly-0.1-SNAPSHOT.jar attempt-manager -c /home/hadoop/.metapipe/conf.json -e test-executor --num-partitions 13000 --executor-job-uri ?attemptUri?
```

Note that many of the parameters need to be tweaked to the cluster setup, especially all the parameters that deal with ram or cores or cpus.
There are probably minimum requirements and "good ranges" for metapipe, but I don't know these yet.

*The `-t` parameter is of vital importance, DO NOT FORGET THAT ONE. It must match the -t parameter you used when running metapipectl.*
