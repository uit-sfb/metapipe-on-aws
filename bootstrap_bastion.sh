#!/bin/bash

# get METAPIPE_ environment variables
source config/env

# format and mount the second EBS volume at /mnt/metapipe, symlink at ~/metapipectl
sudo mkfs -t ext4 /dev/xvdb
sudo mkdir /mnt/metapipe
sudo tee -a /etc/fstab 1> /dev/null <<EOF
/dev/xvdb /mnt/metapipe ext4 defaults 1 2
EOF
sudo mount /mnt/metapipe
sudo chown -R ec2-user:ec2-user /mnt/metapipe
ln -s /mnt/metapipe ~/metapipectl

# mount our EFS share at /mnt/efs
sudo mkdir /mnt/efs
sudo tee -a /etc/fstab 1> /dev/null <<EOF
${METAPIPE_EFS_DNS_NAME}:/ /mnt/efs nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=150,retrans=2,_netdev 0 0
EOF
sudo mount /mnt/efs

# install python3 and rig the local virtualenv
sudo yum install -y python35 python35-virtualenv python35-pip
virtualenv-3.5 pyenv
. ./pyenv/bin/activate
pip install --upgrade pip
pip install -r .pip3-requirements.txt
deactivate

# install java 8 and make it the default (for metapipectl)
sudo yum install -y java-1.8.0
sudo alternatives --set java /usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java

# download metapipectl to ~/metapipectl
cd ~/metapipectl
curl -O -u $METAPIPE_ARTIFACTS_HTTP_AUTH https://artifacts.sfb.uit.no/jenkins/newpan_tools/71/metapipectl-assembly-0.1-SNAPSHOT.jar

# copy metapipectl config files into place
mkdir ~/.metapipe
cd ~/.metapipe
cp -a ~/metapipe-on-aws/config/metapipectl/* .
