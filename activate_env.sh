#!/bin/bash

# activate Python3 virtualenv (in which we have the AWS CLI)
if [[ ! -d ./pyenv ]]
then
    echo "ERROR: no virtual Python3 environment!"
    exit 1
fi
. ./pyenv/bin/activate


# load our environment variables
if [[ -f ./config/env ]]
then
    . ./config/env
else
    echo "WARNING: ./config/env is missing!"
fi


# add SSH key to agent
if [[ ! -f ./config/amazon_emr.pem ]]
then
    echo "ERROR: SSH key file ./config/amazon_emr.pem is missing!"
    exit 1
fi
if [[ -z "$SSH_AUTH_SOCK" ]]
then
    echo "NOTE: we have no SSH agent, so we start one in this shell"
    echo "and add our SSH key."
    echo "Actually, ssh-agent execs another shell, which you might not want."
    echo "The agent will die when the shell dies."
    echo "You can't deactivate the virtualenv now, you'll have to exit"
    echo "the shell first."
    echo ""
    ssh-agent $SHELL -c "ssh-add ./config/amazon_emr.pem; exec $SHELL"
else
    echo "NOTE: adding our SSH key to the SSH agent. No max lifetime!"
    echo ""
    ssh-add ./config/amazon_emr.pem
fi
