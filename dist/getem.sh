#!/bin/bash

if [[ -z "$METAPIPE_ARTIFACTS_HTTP_AUTH" ]]
then
    echo "METAPIPE_ARTIFACTS_HTTP_AUTH missing - did you forget to activate_env?"
    exit 1
fi


# workflow assembly - this is submitted to spark
curl -O -u $METAPIPE_ARTIFACTS_HTTP_AUTH https://artifacts.sfb.uit.no/internal/workflow-assembly-0.1-SNAPSHOT.jar # custom branch with fix...
#curl -O -u $METAPIPE_ARTIFACTS_HTTP_AUTH https://artifacts.sfb.uit.no/jenkins/newpan_tools/67/workflow-assembly-0.1-SNAPSHOT.jar

# metapipectl - command line tool to submit jobs to the metapipe frontend at jobs.metapipe.uit.no
curl -O -u $METAPIPE_ARTIFACTS_HTTP_AUTH https://artifacts.sfb.uit.no/jenkins/newpan_tools/71/metapipectl-assembly-0.1-SNAPSHOT.jar

# executionmanager - this is a command line tool that fetches jobs from jobs.metapipe.uit.no and submits them to spark (it submits workflow assembly)
curl -O -u $METAPIPE_ARTIFACTS_HTTP_AUTH https://artifacts.sfb.uit.no/internal/executionManager-assembly-0.1-SNAPSHOT.jar # custom branch with fix for client deploy mode
#curl -O -u $METAPIPE_ARTIFACTS_HTTP_AUTH https://artifacts.sfb.uit.no/jenkins/newpan_tools/71/executionManager-assembly-0.1-SNAPSHOT.jar

# metapipe dependencies - programs, databases etc.
curl -O -u $METAPIPE_ARTIFACTS_HTTP_AUTH https://artifacts.sfb.uit.no/jenkins/newpan_tools_dependencies/20/metapipe-dependencies.tar.gz
