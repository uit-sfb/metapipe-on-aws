#!/bin/bash

# uploads the metapipe archives to S3, in us-east-2 (Ohio)

aws s3 sync . s3://${METAPIPE_BUCKET}/rigging --exclude '*' --include '*.jar' --include '*.tar.gz'
