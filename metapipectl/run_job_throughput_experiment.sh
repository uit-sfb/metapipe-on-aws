#!/bin/bash

if [[ $# -ne 3 ]]; then
    echo "usage: $0 path_to_dataset workdir number_of_metapipe_jobs_to_submit"
    exit 1
fi

dataset=$1
dataset_stripped=$(basename "$dataset")
workdir=$2
num_jobs=$3

# rig working directory with metapipectl executable and dataset file links
cd_back_to=`pwd`
mkdir -p "$workdir"
cd "$workdir"
ln "${cd_back_to}/${dataset}"
ln "${cd_back_to}/metapipectl-assembly-0.1-SNAPSHOT.jar"
for i in $(seq -w 1 $num_jobs); do
    ln "$dataset_stripped" "_${i}_${dataset_stripped}"
done

# start stopwatch
time_start=`date -R`
ts_start=`date +%s`
echo "START  $num_jobs x $dataset_stripped @ $time_start" | tee -a log.txt

# run metapipectl
metapipe_command="java -jar ./metapipectl-assembly-0.1-SNAPSHOT.jar submit --contigs-cutoff 0 --remove-non-complete-genes true --export-merged-genbank false --force -t metapipe-lars-aws-2 ./_*"
$metapipe_command 2>&1 | tee log.txt

# stop stopwatch
time_finish=`date -R`
ts_finish=`date +%s`
duration=$(($ts_finish - $ts_start))
echo "FINISH $num_jobs x $dataset_stripped @ $time_finish DURATION $duration s" | tee -a log.txt
